# e301 Release Cutter Gradle Plugin

## About
This project is a Gradle Plugin that includes the first process for the NAB e301 release cut process.

Once run, it will:
 1. Check if a release branch exists. If so, abort.
 2. Check all commits on master exist on develop. If some are missing, abort.
 3. Create a remote release branch based on develop.
 4. Increment the version number on develop and push to remote.
 
To run type:
```
gradle clean autorelease
```

Source of related projects
```
Gradle plugin:  https://github.aus.thenational.com/ATDCIB/gradle_auto_release
Jenkins plugin: https://github.aus.thenational.com/ATDCIB/jenkins_auto_release
```

## Setup Jenkins Job
Allows you to perfrom release builds from within Jenkins. To setup:
```
 1. In Jenkins, go to Job Generator
 2. Click on your project
 3. Click Configure
 4. Search for Auto release build and select the tick box
 5. Save
 6. Click Build with parameters and build it.
```

## Install in Service Engine
Make code changes on a technical-debt branch, for example, the current branch used for this Jira ticket assigned to the task is:
technical-debt/DCIB-5634-add-auto-release-cut


Modify build.gradle:

- Get version from property file:
The version is now taken from gradle.properties file. You need to understand how your project uses version. For example, simple payments would:
```
replace:
def buildVersion = '1.12.0'
with:
def buildVersion = versionNumber
```

whereas ese_reference_data would:
```
replace:
version '1.4.0-' + (isJenkinsBuild ? jenkinsSnapShot() : 'local-snapshot')
with:
version versionNumber + '-' + (isJenkinsBuild ? jenkinsSnapShot() : 'local-snapshot')
```

- Add gradle-auto-release to your gradle file
```
buildscript {
    dependencies {
        classpath("com.nab:gradle-auto-release:1.0.1-22")
    }
}

apply plugin: 'com.nab.ese.gradle.autorelease'
```

- Add the following line (with current version number) to gradle.properties (found in the root directory, create if necessary)
```
versionNumber=1.4.0
```

- Remove gradle.properties from .gitignore

- Once reviewed, merge to develop and test by making a release. If happy, delete release branch and also potential artefact that was created in artifactory.