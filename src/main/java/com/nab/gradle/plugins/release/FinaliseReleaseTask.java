package com.nab.gradle.plugins.release;

import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LsRemoteCommand;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.TransportCommand;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.merge.MergeStrategy;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static org.eclipse.jgit.api.MergeResult.MergeStatus.ALREADY_UP_TO_DATE;
import static org.eclipse.jgit.api.MergeResult.MergeStatus.FAST_FORWARD;
import static org.eclipse.jgit.api.MergeResult.MergeStatus.MERGED;

//release comment
public class FinaliseReleaseTask {

  public static void main(String... args) throws Exception {
    FinaliseReleaseTask a = new FinaliseReleaseTask();
    a.finaliseRelease();
  }


  @TaskAction
  public void finaliseRelease() throws Exception {
    final Git git = Git.open(new File("."));

    Config storedConfig = git.getRepository().getConfig();
    String gitRepoUri = storedConfig.getString("remote", "origin", "url");
    storedConfig.setString("user", null, "name", "autobuild");
    storedConfig.setString("user", null, "email", "no-reply@nab.com.au");

    System.out.println("Check if release branch exists remotely...");
    checkIfReleaseAndMasterExist(git, gitRepoUri);
    System.out.println("Release branch exists.");

    // Create push command for all the following tasks
    final PushCommand push = git.push();
    push.setRemote("origin");
    setCredentials(gitRepoUri, push);

    // Checkout release branch
    System.out.println("Checkout release branch...");
    git.checkout()
        .setName("release")
        .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
        .setStartPoint("origin/release")
        .setCreateBranch(true).call();

    System.out.println("Release branch checked out.");

    // create annotated tag
    String currentVersion = getVersion();
    System.out.println("Create annotated tag: v" + currentVersion);
    git
        .tag()
        .setName("v" + currentVersion)
        .setMessage("Release <releaseName>")
        .setAnnotated(true).setForceUpdate(true).call();
    push.setRefSpecs(new RefSpec("refs/remotes/origin/release:refs/remotes/origin/release")).setForce(true);
    push.setPushTags().call();
    System.out.println("Annotated tag pushed.");

    // merge release into master
    doMerge(git, push, "master", "refs/remotes/origin/release");

    // merge master into develop
    doMerge(git, push, "develop", "refs/remotes/origin/master");

    // delete release branch
    System.out.println("Delete release branch...");
    git.branchDelete().setBranchNames("refs/remotes/origin/release").setForce(true).call();
    push.setRefSpecs(new RefSpec().setSource(null).setDestination("refs/remotes/origin/release"));
    push.call();
    System.out.println("Release branch deleted.");
  }

  // merge useless comment and again and THEIRS has gone
  private void doMerge(Git git, PushCommand push, String baseBranch, String branchToMerge) throws Exception {
    System.out.println("Merge " + branchToMerge + " into " + baseBranch + "...");
    CheckoutCommand checkout =
        git.checkout()
            .setName(baseBranch)
            .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
            .setStartPoint("origin/" + baseBranch);
    try {
      checkout.setCreateBranch(true).call();
    } catch (Exception e) {
      checkout.setCreateBranch(false).call();
    }
    System.out.println(baseBranch + " checked out.");

    Ref ref = git.getRepository().findRef(branchToMerge);
    MergeCommand merge = git.merge();
    if (ref != null) {
      merge.include(ref);
    } else {
      merge.include(ObjectId.fromString(branchToMerge));
    }
    merge.setCommit(true);
    merge.setStrategy(MergeStrategy.THEIRS);
    merge.setMessage("Auto release process. Merge " + branchToMerge + " into " + baseBranch);

    MergeResult mergeResult = merge.call();

    System.out.println("Merge result: " + mergeResult.getMergeStatus());
    if (MERGED != mergeResult.getMergeStatus()
        && ALREADY_UP_TO_DATE != mergeResult.getMergeStatus()
        && FAST_FORWARD != mergeResult.getMergeStatus()) {
      throw new Exception("Merge failed with status " + mergeResult.getMergeStatus());
    }

    push.setRefSpecs(new RefSpec(baseBranch + ":" + baseBranch));
    push.call();
    System.out.println(branchToMerge + " merged into " + baseBranch + " successfully.");
  }

  private void checkIfReleaseAndMasterExist(Git git, String gitRepoUri) throws Exception {
    final LsRemoteCommand lsRemote = git.lsRemote();
    lsRemote.setRemote("origin");
    setCredentials(gitRepoUri, lsRemote);

    boolean foundRelease = false;
    boolean foundMaster = false;
    for (Ref ref : lsRemote.call()) {
      if (StringUtils.containsIgnoreCase(ref.getName(), "/release")) {
        foundRelease = true;
      }
      if (StringUtils.containsIgnoreCase(ref.getName(), "/master")) {
        foundMaster = true;
      }
    }

    if (!foundRelease) {
      throw new Exception("Release branch does not exist");
    }
    if (!foundMaster) {
      throw new Exception("Master branch does not exist");
    }
  }

  private String getVersion() throws ConfigurationException {
    PropertiesConfiguration config = new PropertiesConfiguration("gradle.properties");
    String currentVersion;
    try {
      currentVersion = (String) config.getProperty("versionNumber");
    } catch (Exception e) {
      throw new RuntimeException(
          "Make sure build.properties exists with versionNumber property in major.minor.patch format, for example 1.0.1");
    }
    return currentVersion;
  }


  protected void setCredentials(String gitRepoUri, TransportCommand lsRemote) throws Exception {
    if (gitRepoUri.toUpperCase().contains("HTTPS")) {
      String username = "";
      String password = "";
      try {
        username = "davidsartori@gmail.com";
        password = "Dirtbike1";
      } catch (Exception e) {
      }
      lsRemote.setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password));
    } else {
      final AtomicBoolean success = new AtomicBoolean(true);
      lsRemote.setTransportConfigCallback(transport -> {
        if (transport instanceof SshTransport) {
          SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session) {
              session.setProxy(new ProxyHTTP("internal-proxy.account", 8080));
            }
          };
          SshTransport sshTransport = (SshTransport) transport;
          sshTransport.setSshSessionFactory(sshSessionFactory);
        } else {
          success.set(false);
        }
      });

      if (!success.get()) {
        throw new Exception("This box doesn't have permission to push to the remote Git repo");
      }

      lsRemote.setCredentialsProvider(allowHosts);
    }

  }

  private CredentialsProvider allowHosts = new CredentialsProvider() {
    @Override
    public boolean supports(CredentialItem... items) {
      return true;
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
      return true;
    }

    @Override
    public boolean isInteractive() {
      return true;
    }
  };
}