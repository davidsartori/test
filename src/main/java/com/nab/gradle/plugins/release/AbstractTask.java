package com.nab.gradle.plugins.release;

import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.api.TransportCommand;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.gradle.api.DefaultTask;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractTask extends DefaultTask {
  protected void setCredentials(String gitRepoUri, TransportCommand lsRemote) throws Exception {
    if (gitRepoUri.toUpperCase().contains("HTTPS")) {
      String username = "";
      String password = "";
      try {
        username = getProject().property("username") != null ? getProject().property("username").toString() : "";
        password = getProject().property("password") != null ? getProject().property("password").toString() : "";
      } catch (Exception e) {
      }
      lsRemote.setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password));
    } else {
      final AtomicBoolean success = new AtomicBoolean(true);
      lsRemote.setTransportConfigCallback(transport -> {
        if (transport instanceof SshTransport) {
          SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session) {
              session.setProxy(new ProxyHTTP("internal-proxy.account", 8080));
            }
          };
          SshTransport sshTransport = (SshTransport) transport;
          sshTransport.setSshSessionFactory(sshSessionFactory);
        } else {
          success.set(false);
        }
      });

      if (!success.get()) {
        throw new Exception("This box doesn't have permission to push to the remote Git repo");
      }

      lsRemote.setCredentialsProvider(allowHosts);
    }

  }

  private CredentialsProvider allowHosts = new CredentialsProvider() {
    @Override
    public boolean supports(CredentialItem... items) {
      return true;
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
      return true;
    }

    @Override
    public boolean isInteractive() {
      return true;
    }
  };

}
